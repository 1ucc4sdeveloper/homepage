FROM node:13-alpine

WORKDIR /app/homepage

ENV PATH /app/homepage/node_modules/.bin:$PATH

COPY package.json /app/homepage/package.json

RUN npm install

RUN npm run build

EXPOSE 5000

CMD ["npm", "run", "server"]

