import logo from './logo.png';
import Routes from "./routes";

const App = () => <Routes />;

export default App;
